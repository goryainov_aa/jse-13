package ru.goryainov.tm.controller;



import ru.goryainov.tm.entity.User;
import ru.goryainov.tm.service.UserService;

import java.util.HashMap;
import java.util.UUID;


public class UserController extends AbstractController {

    private UserService userService;
    private final HashMap userSession = new HashMap<UUID, User>();

    public UserController(UserService userService) {
        this.userService = userService;
    }

    /**
     * Cоздание пользователя
     */
    public int createUser() {
        System.out.println("[Create user]");
        System.out.println("[Please, enter user firstname:]");
        final String firstName = scanNextLine();
        System.out.println("[Please, enter user lastname:]");
        final String lastName = scanNextLine();
        System.out.println("[Please, enter user middlename:]");
        final String middleName = scanNextLine();
        System.out.println("[Please, enter user login:]");
        final String login = scanNextLine();
        System.out.println("Please, enter user role:");
        String role = scanNextLine();
        System.out.println("Please, enter user password:");
        String pwd = scanNextLine();
        userService.create(firstName, lastName, middleName, login, pwd, role);
        System.out.println("[Ok]");
        return 0;
    }

    /**
     * Изменение пользователя по индексу
     */
    public int updateUserByIndex() {
        System.out.println("[Update user]");
        System.out.println("[Please, enter user index:]");
        final int index = Integer.parseInt(scanNextLine()) - 1;
        final User user = userService.findByIndex(index);
        if (user == null) {
            System.out.println("[Fail]");
            return 0;
        }
        System.out.println("[Please, enter user firstname:]");
        final String firstName = scanNextLine();
        System.out.println("[Please, enter user lastname:]");
        final String lastName = scanNextLine();
        System.out.println("[Please, enter user middlename:]");
        final String middleName = scanNextLine();
        System.out.println("[Please, enter user login:]");
        final String login = scanNextLine();
        System.out.println("[Please, enter user role:]");
        final String role = scanNextLine();
        userService.update(user.getId(), firstName, lastName, middleName, login, role);
        System.out.println("[Ok]");
        return 0;
    }

    /**
     * Изменение пользователя по идентификатору
     */
    public int updateUserById() {
        System.out.println("[Update user]");
        System.out.println("[Please, enter user id:]");
        final long id = Long.parseLong(scanNextLine());
        final User user = userService.findById(id);
        if (user == null) {
            System.out.println("[Fail]");
            return 0;
        }
        System.out.println("[Please, enter user firstname:]");
        final String firstName = scanNextLine();
        System.out.println("[Please, enter user lastname:]");
        final String lastName = scanNextLine();
        System.out.println("[Please, enter user middlename:]");
        final String middleName = scanNextLine();
        System.out.println("[Please, enter user login:]");
        final String login = scanNextLine();
        System.out.println("[Please, enter user role:]");
        final String role = scanNextLine();
        userService.update(user.getId(), firstName, lastName, middleName, login, role);
        System.out.println("[Ok]");
        return 0;
    }

    /**
     * Изменение пользователя по логину
     */
    public int updateUserByLogin() {
        System.out.println("[Update user]");
        System.out.println("[Please, enter user login:]");
        final String login = scanNextLine();
        final User user = userService.findByLogin(login);
        if (user == null) {
            System.out.println("[Fail]");
            return 0;
        }
        System.out.println("[Please, enter user firstname:]");
        final String firstName = scanNextLine();
        System.out.println("[Please, enter user lastname:]");
        final String lastName = scanNextLine();
        System.out.println("[Please, enter user middlename:]");
        final String middleName = scanNextLine();
        System.out.println("[Please, enter user role:]");
        final String role = scanNextLine();
        userService.update(user.getId(), firstName, lastName, middleName, login, role);
        System.out.println("[Ok]");
        return 0;
    }

    /**
     * Удаление пользователя из списка по имени
     */
    public int removeUserByName() {
        System.out.println("[Remove user by name]");
        System.out.println("[Please, enter user firstname:]");
        final String firstName = scanNextLine();
        System.out.println("[Please, enter user lastname:]");
        final String lastName = scanNextLine();
        System.out.println("[Please, enter user middlename:]");
        final String middleName = scanNextLine();
        final User user = userService.removeByName(firstName, lastName, middleName);
        if (user == null) System.out.println("[Fail]");
        else System.out.println("[Ok]");
        return 0;
    }

    /**
     * Удаление пользователя из списка по идентификатору
     */
    public int removeUserById() {
        System.out.println("[Remove user by id]");
        System.out.println("[Please, enter user id:]");
        final long id = scanNextLong();
        final User user = userService.removeById(id);
        if (user == null) System.out.println("[Fail]");
        else System.out.println("[Ok]");
        return 0;
    }

    /**
     * Удаление пользователя из списка по логину
     */
    public int removeUserByLogin() {
        System.out.println("[Remove user by login]");
        System.out.println("[Please, enter user login:]");
        final String login = scanNextLine();
        final User user = userService.removeByLogin(login);
        if (user == null) System.out.println("[Fail]");
        else System.out.println("[Ok]");
        return 0;
    }

    /**
     * Удаление пользователя из списка по индексу
     */
    public int removeUserByIndex() {
        System.out.println("[Remove user by index]");
        System.out.println("[Please, enter user index:]");
        final int index = scanNextInt() - 1;
        final User user = userService.removeByIndex(index);
        if (user == null) System.out.println("[Fail]");
        else System.out.println("[Ok]");
        return 0;
    }

    /**
     * Очистки проекта
     */
    public int clearUser() {
        System.out.println("[Clear user]");
        userService.clear();
        System.out.println("[Ok]");
        return 0;
    }

    /**
     * Просмотр списка пользователей
     */
    public void viewUser(final User user) {
        if (user == null) return;
        System.out.println("[View user]");
        System.out.println("Id: " + user.getId());
        System.out.println("FirstName: " + user.getFirstName());
        System.out.println("LasttName: " + user.getLastName());
        System.out.println("MiddleName: " + user.getMiddleName());
        System.out.println("Login: " + user.getLogin());
        System.out.println("Role: " + user.getRole());
        System.out.println("[Ok]");
    }

    /**
     * Просмотр пользователя по индексу
     */
    public int viewUserByIndex() {
        System.out.println("Enter, user index: ");
        final int index = scanNextInt() - 1;
        final User user = userService.findByIndex(index);
        viewUser(user);
        return 0;
    }

    /**
     * Просмотр пользователя по идентификатору
     */
    public int viewUserById() {
        System.out.println("Enter, user id: ");
        final Long id = scanNextLong();
        final User user = userService.findById(id);
        viewUser(user);
        return 0;
    }

    /**
     * Просмотр пользователя по логину
     */
    public int viewUserByLogin() {
        System.out.println("Enter, user login: ");
        final String login = scanNextLine();
        final User user = userService.findByLogin(login);
        viewUser(user);
        return 0;
    }

    /**
     * Вывод списка пользователей
     */
    public int listUser() {
        System.out.println("[List user]");
        int index = 1;
        for (final User user : userService.findAll()) {
            System.out.println(Integer.toString(index) + '.');
            viewUser(user);
            index++;
        }
        /*System.out.println("[Ok]");*/
        return 0;
    }

    /**
     * Логон пользователя
     */
    public UUID authentification() {
        System.out.println("Logon");
        System.out.println("Please, enter login...");
        String login = scanNextLine();
        System.out.println("Please, enter password...");
        String password = scanNextLine();
        User user = userService.authentification(login, password);
        if (user != null) {
            UUID sessionId = UUID.randomUUID();
            userSession.put(sessionId, user);
            System.out.println("Authentication succeeded!");
            System.out.println("session id:" + sessionId);
            return sessionId;
        }
        System.out.println("Authentication failed!");
        return null;
    }


    /**
     * Логоф пользователя
     */
    public void logOff() {
        System.out.println("Log Off");
        System.out.println("Please, enter session id...");
        UUID sessionId = UUID.fromString(scanNextLine());
        if (sessionId != null) {
            if (userSession.remove(sessionId) != null) {
                System.out.println("Log Off session " + sessionId.toString() + "succeeded!");
                System.out.println();
                return;
            }
            System.out.println("Log Off failed! Unknown session:" + sessionId.toString());
            return;
        }
        System.out.println("Log Off failed! ");
    }

    /**
     * Проверка аторизации пользователя
     */
    public boolean checkSession(UUID sessionId) {
        if (userSession.containsKey(sessionId)) return true;
        System.out.println("Access denied! Please, log in...");
        return false;
    }

    /**
     * Поиск пользователя по сессии
     */
    public User getUserBySession(UUID sessionId) {
        User user = (User) userSession.getOrDefault(sessionId, null);
        if (user != null) return user;
        System.out.println("Access denied! Please, log in...");
        return null;
    }

    /**
     * Поиск пользователя по id
     */
    public User getUserById(Long userId) {
        User user = userService.findById(userId);
        if (user != null) return user;
        return null;
    }

    /**
     * Поиск пользователя по login
     */
    public User getUserByLogin(String userLogin) {
        User user = userService.findByLogin(userLogin);
        if (user != null) return user;
        return null;
    }

    /**
     * Смена пароля пользователя
     */
    public void changePasswordByUserLogin(final UUID sessionId) {
        if (!checkSession(sessionId)) return;
        User userByLogin = getUserBySession(sessionId);
        if (userByLogin == null) return;
        System.out.println("Change user password");
        System.out.println("Please, enter user login...");
        String login = scanNextLine();
        if (login == null || login.isEmpty()) return;
        User userChange = getUserByLogin(login);
        if (userByLogin.getId() == userChange.getId() || checkAdminGrants(userByLogin))  {
            System.out.println("Please, enter new password...");
            String passwordChange = scanNextLine();
            userChange.setPasswordHash(passwordChange);
            System.out.println("Ok");
            return;
        }
        System.out.println("You don't have enough privileges!");
    }
}
